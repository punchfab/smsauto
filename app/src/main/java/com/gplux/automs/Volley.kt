package com.gplux.automs

import android.content.Context
import android.widget.Toast
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONException
import org.json.JSONObject
import android.app.Application
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest


class Volley(private val url: String) {

    fun getData(context: Context, body: (JSONObject) -> Unit) {
        //"https://kypo.gameplux.com/api.php?op=getAll"
        val stringRequest = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->
                try {
                    val obj = JSONObject(response)
                    body(obj)
                    Toast.makeText(context, obj.getString("msg"), Toast.LENGTH_LONG).show()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener {
                    volleyError->
                Toast.makeText(context, volleyError.message, Toast.LENGTH_LONG).show()})

        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }

    fun sendData(context: Context, getParams: (JSONObject) -> Unit = {}, setParams: () -> Map<String,String> = {HashMap()}) {

        val request = object : StringRequest(Method.POST, url,
            Response.Listener { response ->
                try {
                    getParams(JSONObject(response))
                    Toast.makeText(context, "$response", Toast.LENGTH_LONG).show()
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(context, error.message, Toast.LENGTH_LONG).show()
            })
        {
            override fun getBodyContentType(): String {
                return "application/json"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                //val jsonObject = JSONObject(setParams())
                //val params = HashMap<String, String>()
                return JSONObject(setParams()).toString().toByteArray()
            }
        }
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            0,
            1f)

        VolleySingleton.getInstance(context).addToRequestQueue(request)
    }
}

class VolleySingleton constructor(context: Context) {
    /*override fun onCreate() {
        super.onCreate()
        instance = this
    }*/
    companion object {
        @Volatile
        private var INSTANCE: VolleySingleton? = null
        fun getInstance(context: Context) =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: VolleySingleton(context).also {
                    INSTANCE = it
                }
            }
    }

    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context.applicationContext)
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        requestQueue?.add(request)
    }


}

