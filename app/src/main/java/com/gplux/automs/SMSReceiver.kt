package com.gplux.automs

import android.annotation.TargetApi
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.SmsMessage
import android.util.Log
import android.widget.Toast
import java.util.*
import com.google.gson.GsonBuilder

class SMSReceiver : BroadcastReceiver() {

    private val TAG: String  = SMSReceiver::class.java.name
    private val pduType: String = "pdus"
    private val gson = GsonBuilder().setPrettyPrinting().create()
    private var codeBank: String = ""
    private val entitiesNumbers = HashMap<String,String>()

    @TargetApi(Build.VERSION_CODES.M)
    override fun onReceive(context: Context, intent: Intent) {
        entitiesNumbers["299999"] = "pichincha"
        entitiesNumbers["8080"] = "guayaquil"
        val bundle = intent.extras
        //val intentExtras = intent.extras
        val subId = bundle!!.getInt("subscription", -1)
        val sms = bundle.get(pduType) as Array<Any>

        var strMessage: String = ""

        //val pdus = bundle!!.get(pdu_type) as Array<Any>?



        if (sms != null) {
            //Check the Android Version
            val isVersionM: Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            val msg = arrayOfNulls<SmsMessage>(sms!!.size)
            val c = msg.size - 1

            for (index in 0 .. c) {
                val format = bundle.getString("format")
                // Check Android version and use appropriate createFromPdu.
                val smsMessage = when (isVersionM) {
                    // If Android version M or newer:
                    true -> SmsMessage.createFromPdu(sms[index] as ByteArray, format)
                    // If Android version L or older:1
                    false -> SmsMessage.createFromPdu(sms[index] as ByteArray)
                }

                // Build the message to show.
                var smsBody = smsMessage.messageBody.toString()
                var address = smsMessage.originatingAddress
                var details = " slot:" + bundle.getInt("slot", -1) +
                              " subscription:" + bundle.getInt("subscription", -1)
                //strMessage += "SMS from " + msgs[index]!!.originatingAddress
                //strMessage += " :" + msgs[index]!!.messageBody + "\n"
                // Log and display the SMS message.
                Log.d(TAG, "onReceive: $details")
                if (address == "299999" || address == "8080") {
                    codeBank = containsNumbers(smsBody)
                    Volley("http://138.197.109.37:3000/val/${entitiesNumbers[address]}/$codeBank").getData(context){}
                Toast.makeText(context, "$address: $details", Toast.LENGTH_LONG).show()
                }
            }
        }

    }

    private fun containsNumbers( text: String, position: Int = 1, quantity: Int = 5): String {

        var code = ""
        var i = position
        var q = quantity + position
        for ( c in text ) {
            if ( c in '0'..'9' && i <= q) {
                code += c
                i++
            }
        }

        return code
    }
}
