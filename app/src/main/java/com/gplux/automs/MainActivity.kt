package com.gplux.automs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var msg = "Estimado Cliente, el codigo de seguridad de tu transaccion es: 253802.Tiempo duracion: 2 minutos.Banco Pichincha: 07/Nov a las 22:17"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvRex.text = containsNumbersRegex(msg)
    }

    private fun containsNumbersRegex( text: String, position: Int = 1, quantity: Int = 5): String {

        var code = ""
        var i = position
        var q = quantity + position
        for ( c in text ) {
            if ( c in '0'..'9' && i <= q) {
                code += c
                i++
            }
        }

        return code
    }
}
